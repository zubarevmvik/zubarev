package lesson2;

public class Lesson2 {
    public static void main(String[] args) {
//        Задание 1
//        переменная int count, начальное значение можно указать любое.
//        Напишите программу, которая выводит на экран count в степени 10, если count является чётным числом, и count в степени 3, если count нечётное.
        System.out.println("Задание 1");
        int count;
        count = 233;

        if (count % 2 == 0) {
            System.out.println(Math.pow(count, 10));
        } else {
            System.out.println(Math.pow(count, 2));
        }


//        Задание 2
//        целочисленное n, начальное значение можно указать любое.
//        Найти количество натуральных чисел, не превосходящих n и не делящихся ни на одно из чисел 2, 3, 5.
        System.out.println();
        System.out.println("Задание 2");
        int n = 300;
        int m = 0;
        int sum = 0;
        while (n >= m) {
            if ((m % 2 == 0) && (m % 3 == 0) && (m % 5 == 0)) {
                sum++;
            }
            m++;
        }
        System.out.println(sum);


//        Задание 3
//        строка str,  начальное значение можно указать любое.//
//        Напишите программу, которая удаляет в строке все лишние пробелы, то есть серии подряд идущих пробелов заменяет на одиночные пробелы. Крайние пробелы в строке также должны удалиться.
//        Пример: str = " привет,   в этой    строке лишние   пробелы.  "
//        Результат: "привет, в этой строке лишние пробелы"
        System.out.println();
        System.out.println("Задание 3");
        String str = " привет,   в этой    строке лишние   пробелы.  ";
        StringBuilder builder = new StringBuilder();
        boolean value = false;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != ' ') {
                builder.append(str.substring(i, i + 1));
                value = true;
            } else {
                if (str.charAt(i) == ' ' && value) {
                    builder.append(" ");
                    value = false;
                }
            }
        }

        System.out.println(builder.toString());


//                Задание 4
//        Дано: целочисленное число n, начальное значение может быть любое.*
//        Напишите программу, которая определяет: (можно сделать не все варианты, а выбрать понравившийся)
//        а) количество цифр в нем;
//        б) сумму его цифр;
//        в) произведение его цифр;
//        г) среднее арифметическое его цифр;
//        д) сумму квадратов его цифр;
//        е) сумму кубов его цифр;
//        ж) его первую цифру;
//        з) сумму его первой и последней цифр.
        System.out.println();
        System.out.println("Задание 4");
        int n2 = 123456789;
        int sum2 = 0;
        int chisel = 0;
        int proizved = 1;
        int kvadrat = 0;
        int kubi = 0;
        int peravaya = 0;
        int poslednaya = n2 % 10;
        while (n2 > 0) {
            chisel++; // а) количество цифр в нем;
            sum2 = sum2 + n2 % 10; //б) сумму его цифр;
            proizved = proizved * n2 % 10; // в) произведение его цифр;
            kvadrat = kvadrat + (n2 % 10) * (n2 % 10); // д) сумму квадратов его цифр;
            kubi = kubi + (int) Math.pow(n2 % 10, 3); // е) сумму кубов его цифр;
            peravaya = n2 % 10; // ж) его первую цифру;
            n2 = n2 / 10;
        }
        System.out.println(chisel + " количество чисел");
        System.out.println(sum2 + " сумма чисел");
        System.out.println(proizved + " произведение чисел");
        System.out.println(sum2 / chisel + " среднее арифметическое"); //г) среднее арифметическое его цифр;
        System.out.println(kvadrat + " Сумма квадратов");
        System.out.println(kubi + " Сумма кубов");
        System.out.println(peravaya + " Первая цифра");
        System.out.println(peravaya + poslednaya + " Сумма первой и последней цифры"); // з) сумму его первой и последней цифр.


//        Задание 5
//        Дано: строка str, начальное значение может быть любое.
//        Напишите программу, считающую количество цифр 1, 2, 3 в строке.
//        Пример: str = "сегодня мы купили 1 яблоко, 1 грушу и 2 апельсина"
//        Кол-во 1: 2
//        Кол-во 2: 1
//        Кол-во 3: 0


        System.out.println();
        System.out.println("Задание 5");
        String str5 = "сегодня мы купили 1 яблоко, 1 грушу и 2 апельсина и 33 мандарина";
        int one = 0;
        int two = 0;
        int three = 0;

        for (int i = 0; i < str5.length(); i++) {
            switch (str5.charAt(i)) {
                case '1':
                    one++;
                    break;
                case '2':
                    two++;
                    break;
                case '3':
                    three++;
                    break;
            }
        }
        System.out.println(one);
        System.out.println(two);
        System.out.println(three);
    }
}
