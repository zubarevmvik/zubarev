package lesson6.task3;

/**
 * Вы реализуете метод - division с аргументами a и b, где a / b.
 * В случае, если вы делите на ноль - отловите ошибку и напишите в консоль, что произошло деление на ноль.
 */
public class Main {
    public static void main(String[] args) {
        int a = 41;
        int b = 0;
        System.out.println(division(a, b));
    }

    public static int division(int a, int b) {
        int d = 0;
        try {
            d = a / b;
        } catch (ArithmeticException exception) {
            System.out.println("произошло деление на ноль");
        }
        return d;
    }
}