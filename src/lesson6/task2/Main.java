package lesson6.task2;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Напишите общий метод для обмена позициями двух разных элементов в массиве.
 * Аргументы дженерик метода: список с элементами типа Type, индекс элемента a, индекс элемента b.
 * Результат - поменять значения элементов a и b местами в списке.
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        list = filtrationEvenNumbers(list, 0, 2);

        for (Integer list1 : list) {
            System.out.print(list1 + " ");
        }
    }

    private static ArrayList<Integer> filtrationEvenNumbers(List<? extends Number> sort, int a, int b) {
        ArrayList<Integer> sortNew = (ArrayList<Integer>) sort;
        Integer temporary = sortNew.get(a);
        sortNew.set(a, sortNew.get(b));
        sortNew.set(b, temporary);
        return sortNew;
    }
}