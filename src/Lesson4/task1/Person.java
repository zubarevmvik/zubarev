package Lesson4.task1;

/*
 * Создать класс Person, который содержит:
 * a) поля fullName, age.
 * б) методы move() и talk(), в которых просто вывести на консоль сообщение -"Такой-то  Person говорит".
 * в) Добавьте два конструктора - Person() и Person(fullName, age).
 * Создайте два объекта этого класса (в методе main). Один объект инициализируется конструктором Person(), другой - Person(fullName, age).
 * Для удобства рекомендуется сделать папку lesson4 внутри которой сделать папку task1 и создать отдельно класс Main и класс Person (как на практике).
 */
public class Person {
    private static final int AGE = 18;
    private String fullName;

    public Person(String fullName) {
        this.fullName = fullName;
    }

    public Person() {
        this.fullName = "Неизвестный";
    }

    public void talk(String name) {
        System.out.println("Такой-то " + name + " говорит");
    }

    public void move(String name) {
        System.out.println("Такой-то " + name + " двигается");
    }
}