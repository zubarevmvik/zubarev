package Lesson4.task2;

public class Main {
    private static final char FIRSTDIGITOFCREDITCARD = '5';

    public static void main(String[] args) {
        Buyer[] buyer = new Buyer[5];
        buyer[0] = new Buyer("Смирнов", "Сергей", "", "адрес1", "4231", 4321);
        buyer[1] = new Buyer("Сидоров", "Иван", "Евгеньевич", "адрес2", "1232", 4321);
        buyer[2] = new Buyer("Иванов", "Николай", "Иванович", "адрес3", "5233", 4321);
        buyer[3] = new Buyer("Александров", "Евгений", "Евгеньевич", "адрес4", "1234", 4321);
        buyer[4] = new Buyer("Антонов", "Семен", "Сергеевич", "адрес5", "5235", 4321);

        int indexMaximumName = 0;
        for (int i = 0; i < buyer.length - 1; i++) {
            if (buyer[indexMaximumName].getSurNameLength() < buyer[i + 1].getSurNameLength()) {
                indexMaximumName = i + 1;
            }
        }

        System.out.println(buyer[indexMaximumName].getName());
        for (int i = 0; i < buyer.length - 1; i++) {
            if (buyer[i].getCreditCardNumber().charAt(0) == FIRSTDIGITOFCREDITCARD) {
                System.out.println(buyer[i].getTheAddress());
            }
        }

        for (int i = 0; i < buyer.length - 1; i++) {
            if (buyer[i].getPatronymic().equals("Евгеньевич")) {
                System.out.println("Покупатель " + buyer[i].getSurname() + " " + buyer[i].getName() + " " + buyer[i].getPatronymic());
            }
        }
    }
}