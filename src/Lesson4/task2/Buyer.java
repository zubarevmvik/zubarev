package Lesson4.task2;
/**
 * Создать класс покупатель с полями: фамилия, имя, отчество, адрес, номер кредитной карточки, номер банковского счета.
 * <p>
 * Создать список из 5 покупателей (в main).
 * Вывести:
 * <p>
 * Имя покупателя с самой длинной фамилией
 * Адреса всех покупателей, у кого первая цифра номера кредитки 5.
 * Всех покупателей с отчеством "Евгеньевич"
 */

/*
 * surname - фамилия
 * name - имя
 * patronymic - отчество
 * theAddress - адрес
 * creditCardNumber - номер кредитной карточки
 * BankAccountNumber - номер банковского счета.
 */
public class Buyer {
    private String surname;
    private String name;
    private String patronymic;
    private String theAddress;
    private String creditCardNumber;
    private int bankAccountNumber;

    public Buyer(String surname, String name, String patronymic, String theAddress, String creditCardNumber, int BankAccountNumber) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.theAddress = theAddress;
        this.creditCardNumber = creditCardNumber;
        this.bankAccountNumber = BankAccountNumber;
    }

    public int getSurNameLength() {
        return this.surname.length();
    }

    public String getSurname() {
        return this.surname;
    }

    public String getName() {
        return this.name;
    }

    public String getPatronymic() {
        return this.patronymic;
    }

    public String getCreditCardNumber() {
        return this.creditCardNumber;
    }

    public String getTheAddress() {
        return this.theAddress;
    }
}