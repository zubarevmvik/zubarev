
package Lesson4.task3;
/**
Создать класс Reader, хранящий такую информацию о пользователе библиотеки: ФИО, номер читательского билета, факультет, дата рождения, телефон. Методы takeBook(), returnBook().

        Разработать программу, в которой создается массив объектов данного класса. Перегрузить методы takeBook(), returnBook():
        1. takeBook, который будет принимать количество взятых книг. Выводит на консоль сообщение "Петров В. В. взял 3 книги".
        2.  takeBook, который будет принимать переменное количество названий книг. Выводит на консоль сообщение "Петров В. В. взял книги: Приключения, Словарь, Энциклопедия".
        3. takeBook, который будет принимать переменное количество объектов класса Book (создать новый класс, содержащий имя и автора книги). Выводит на консоль сообщение "Петров В. В. взял книги: Приключения, Словарь, Энциклопедия".
        Аналогичным образом перегрузить метод returnBook(). Выводит на консоль сообщение "Петров В. В. вернул книги: Приключения, Словарь, Энциклопедия". Или "Петров В. В. вернул 3 книги".

 */

/*
 * fullName - ФИО
 * libraryCardNumber - номер читательского билета
 * faculty - факультет
 * DateOfBirth - дата рождения
 * telephone - телефон
 */

public class Reader {
    private String fullName;
    private int libraryCardNumber;
    private String faculty;
    private String dateOfBirth;
    private String telephone;

    public Reader(String fullName, int libraryCardNumber, String faculty, String dateOfBirth, String telephone) {
        this.fullName = fullName;
        this.libraryCardNumber = libraryCardNumber;
        this.faculty = faculty;
        this.dateOfBirth = dateOfBirth;
        this.telephone = telephone;
    }

    public void takeBook(int numberOfBooks) {
        System.out.println("Петров В. В. взял " + numberOfBooks + " книги");
    }

    public void returnBook(int numberOfBooks) {
        System.out.println("Петров В. В. вернул " + numberOfBooks + " книги");
    }

    public void takeBook(String... books) {
        System.out.print("Петров В. В. взял книги: ");
        boolean skippingFirstComma = false;
        for (String book : books) {
            if (skippingFirstComma) System.out.print(", ");
            System.out.print(book);
            skippingFirstComma = true;
        }
        System.out.println();
    }

    public void returnBook(String... books) {
        System.out.print("Петров В. В. вернул книги: ");
        boolean skippingFirstComma = false;
        for (String book : books) {
            if (skippingFirstComma) System.out.print(", ");
            System.out.print(book);
            skippingFirstComma = true;
        }
        System.out.println();
    }

    public void takeBook(Book... book) {
        System.out.print("Петров В. В. взял книги: ");
        for (Book books : book) {
            System.out.print("Автор - " + books.getAuthor() + " " + books.getNameOfBook() + ";");
        }
        System.out.println();
    }

    public void returnBook(Book... book) {
        System.out.print("Петров В. В. вернул книги: ");
        for (Book books : book) {
            System.out.print("Автор - " + books.getAuthor() + " " + books.getNameOfBook() + ";");
        }
        System.out.println();
    }

}