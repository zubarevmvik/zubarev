package Lesson4.task3;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Book> books = new ArrayList<>(5);
        books.add(new Book("Властелин колец", "Джон Р. М. Толкин"));
        books.add(new Book("Гордость и предубеждение", "Джейн Остин"));
        books.add(new Book("Тёмные начала", "Филип Пулман"));

        List<Reader> readers = new ArrayList<>(4);
        readers.add(new Reader("Шубин Велорий Тарасович", 123, "философский", "01.03.2001", "7926456915"));
        readers.add(new Reader("Фомичёв Иван Давидович", 234, "психологии", "15.08.2004", "79165942648"));
        readers.add(new Reader("Самсонов Демьян Семенович", 345, "экономический", "05.09.2009", "79341258745"));
        readers.add(new Reader("Хохлов Нисон Феликсович", 456, "политологии", "24.03.2008", "123456789"));

        readers.get(0).takeBook(2);
        readers.get(0).returnBook(2);

        readers.get(1).takeBook("Приключения", "Словарь", "Энциклопедия");
        readers.get(1).returnBook("Приключения", "Словарь", "Энциклопедия");

        readers.get(2).takeBook(books.get(0), books.get(1));
        readers.get(2).returnBook(books.get(0), books.get(1));
    }
}