package Lesson4.task3;

/**
 * nameOfBook - название книги
 * author - автор
 */
public class Book {
    private String author;
    private String nameOfBook;

    public Book(String nameOfBook, String author) {
        this.author = author;
        this.nameOfBook = nameOfBook;
    }

    public String getAuthor() {
        return this.author;
    }

    public String getNameOfBook() {
        return this.nameOfBook;
    }
}