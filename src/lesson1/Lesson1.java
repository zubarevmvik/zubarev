package lesson1;
//*
public class Lesson1 {
    public static void main(String[] args) {
//          задание №1
//          Напиши программу, которая выводит на экран надпись: "Мы обязательно изучим Java".
        System.out.println("Задание 1");
        System.out.println("\"Мы обязательно изучим Java\"");


//          Задание №2
//          Напиши программу, которая в методе main объявляет такие переменные: count типа byte, age типа int и starts типа long.
//          Примечание: "объявить переменную" - значит то же, что и "создать переменную".
        System.out.println();
        System.out.println("Задание 2");
        byte count = 1;
        int age = 2;
        long starts = 3;
        System.out.println(count);
        System.out.println(age);
        System.out.println(starts);


//          задание №3
//          Поместите код ниже в метод main и закомментируй ненужные строки кода, чтобы на экран вывелась надпись:
//          2 плюс 3 равно 5
        System.out.println();
        System.out.println("Задание 3");
        int a = 3;
        int b = 2;
        //System.out.print("два");
        System.out.print(b);
        System.out.print(" плюс ");
        //System.out.print(" минус ");
        System.out.print(a);
        //System.out.print("три");
        System.out.print(" равно ");
        //System.out.print(" будет ");
        //System.out.print("пять");
        System.out.print(a + b);
        System.out.println();


        //задание №4
        //Выведите на экран диапазоны значений каждого из целочисленных примитивных типов
        System.out.println();
        System.out.println("Задание 4");
        short a1 = Short.MIN_VALUE;
        short a2 = Short.MAX_VALUE;
        char b1 = Character.MIN_VALUE;
        char b2 = Character.MAX_VALUE;
        int c1 = Integer.MIN_VALUE;
        int c2 = Integer.MAX_VALUE;
        long d1 = Long.MIN_VALUE;
        long d2 = Long.MAX_VALUE;
        System.out.println(a1);
        System.out.println(a2);
        System.out.println(b1);
        System.out.println(b2);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(d1);
        System.out.println(d2);


//          задание №5
//          Реализуйте все месяца года (январь, февраль ..) в рамках Java программы и напечатайте месяц своего рождения.
        System.out.println();
        System.out.println("Задание 5");
        System.out.println(Month.JANUARY);
    }


}
