package lesson3;

import java.util.*;

public class Lesson3 {
    public static void main(String[] args) {
        commonLength();
        queue(2, 15, 10);
        duplicateElementsInAnArray();
        productBalanceAtTheEndOfTheMonth();
    }

    //        Задание 1
//        Для двух отсортированных массивов nums1 и nums2  вернуть медианное значение двух отсортированных массивов.
//        Пример 1:
//        Ввод: nums1 = [1,3], nums2 = [2]
//        Вывод: 2.00000
//        Объяснение: объединенный массив = [1,2,3], а медиана равна 2.
//        Пример 2:
//        Ввод: nums1 = [1,2], nums2 = [3,4]
//        Вывод: 2.50000
//        Объяснение: объединенный массив = [1,2,3,4], а медиана равна (2 + 3) / 2 = 2,5.
    public static void commonLength() {
        System.out.println("Задание 1");
        int nums1[] = {4, 7, 9, 0, 1, 3, 55, 4};
        Integer[] nums2 = new Integer[2];
        nums2[0] = 2;
        nums2[1] = 2;
        //объединяем наши масивы
        int commonLength = nums1.length + nums2.length;
        Integer[] numGeneral = new Integer[commonLength];
        for (int i = 0; i < commonLength; i++) {
            if (i < nums1.length) { //собрали 1 массив
                numGeneral[i] = nums1[i];
            } else { //собираю 2 массив
                numGeneral[i] = nums2[i - nums1.length];
            }
        }
//        сортируем новый масив
        Integer temporary[] = new Integer[1];
        do {
            temporary[0] = null;
            for (int i = 0; i < commonLength - 1; i++) {

                if (numGeneral[i] > numGeneral[i + 1]) {
                    temporary[0] = numGeneral[i];
                    numGeneral[i] = numGeneral[i + 1];
                    numGeneral[i + 1] = temporary[0];
                }
            }
        } while (!(temporary[0] == null));
//        вычисляем медиану для нового массива
        if (numGeneral.length % 2 == 0) { //четное
            double numTotalMedian = (((double) numGeneral[numGeneral.length / 2 - 1]) + (numGeneral[numGeneral.length / 2])) / 2;
            System.out.println(numTotalMedian);
        } else { //не четное
            int numTotalMedian = numGeneral[numGeneral.length / 2];
            System.out.println(numTotalMedian);
        }
    }

    //      Задание 2
//    В отделении банка работает K окон, в общей очереди стоит N человек, каждому из которых понадобится 10 минут на обслуживание. Вывести всех посететителей, кто подойдет к окнам через M минут от начала смены.
//        Пример:
//        Очередь N {Миша, Петя, Катя, Игорь}
//        Окно K = 2
//        M = 15 минут
//        Ответ: на 15 минуте у окон будут стоять Катя и Игорь
    public static void queue(int a, int b, int c) {
        System.out.println("Задание 2");
        ArrayList<String> nQueue = new ArrayList<>();
        int windows = a;
        int currentTime = b;
        int time = c;
        nQueue.add("Миша");
        nQueue.add("Петя");
        nQueue.add("Катя");
        nQueue.add("Игорь");
        while (currentTime > time) {
            for (int i = 0; i < windows; i++) {
                if (!nQueue.isEmpty()) {
                    nQueue.remove(0);
                }
            }
            time += 10;
        }
        for (int i = 0; i < windows; i++) {
            if (!nQueue.isEmpty()) {
                System.out.println(nQueue.get(i));
            } else {
                System.out.println("в очереди ни кого нет");
                break;
            }
        }
    }

    //        Задание 3
//        Подсчитайте повторяющиеся элементы в массиве с помощью HashMap.
//
    public static void duplicateElementsInAnArray() {
        System.out.println("Задание 3");
        HashMap<Integer, String> n = new HashMap<>();
        n.put(1, "1");
        n.put(2, "один");
        n.put(3, "2");
        n.put(4, "3");
        n.put(5, "2");
        n.put(6, "3");
        n.put(7, "3");
        n.put(8, "привет");
        n.put(9, "привет");
        n.put(10, "привет");
        HashMap<String, Integer> answer = new HashMap<>();
        for (
                int i = 1; i < n.size(); i++) {
            if (!answer.containsKey(n.get(i))) { //проверяем что текущий элемент еще не сравнивали
                int amount = 1;
                for (int j = 1; j <= n.size(); j++) {
                    if (i != j && n.get(i).equals(n.get(j))) {
                        amount++;
                        answer.put(n.get(i), amount); //кладем ответ в формате: ключь=элемен, значение=количество повторений элементов
                    }
                }
            }
        }
        System.out.println(answer.entrySet());
    }

//        Задание 4
//                Посчитайте остаток продуктов на конец месяца, имея данные о количестве на начало месяца и ежедневный расход.
//                Пример:
//        Начало месяца {"Картошка": 30, "Перец": 20, "Кукуруза": 80}
//        Расход за месяц:
//        1 день - {"Картошка": 0, "Перец": 1, "Кукуруза": 0}
//        2 день - {"Картошка": 2, "Перец": 1, "Кукуруза": 0}
//        ..
//        31 день - {"Картошка": 4, "Перец": 0, "Кукуруза": 0}
//        Ответ: остаток  {"Картошка": 24, "Перец": 18, "Кукуруза": 80}
//        Подсказка: данные за месяц имеют тип данных List<HashMap<String, Integer>>

    public static void productBalanceAtTheEndOfTheMonth() {
        System.out.println("Задание 4");
        List<HashMap<String, Integer>> beginningOfTheMonth = new ArrayList<>();
        beginningOfTheMonth.add(new HashMap<>() {
            {
                put("Картошка", 30);
                put("Перец", 20);
                put("Кукуруза", 80);
            }
        });
        List<HashMap<String, Integer>> monthlyExpenses = new ArrayList<>();
        monthlyExpenses.add(new HashMap<>() {
            {
                put("Картошка", 0);
                put("Перец", 1);
                put("Кукуруза", 0);
            }
        });
        monthlyExpenses.add(new HashMap<>() {
            {
                put("Картошка", 2);
                put("Перец", 1);
                put("Кукуруза", 0);
            }
        });
        monthlyExpenses.add(new HashMap<>() {
            {
                put("Картошка", 4);
                put("Перец", 0);
                put("Кукуруза", 0);
            }
        });
        int potato = 0;
        int pepper = 0;
        int corn = 0;
        for (
                Map<String, Integer> map : monthlyExpenses) {
            potato = potato + map.get("Картошка");
            pepper = pepper + map.get("Перец");
            corn = corn + map.get("Кукуруза");
        }
        List<HashMap<String, Integer>> atTheEndOfTheMonth = new ArrayList<>();
        int finalPotato = potato;
        int finalPepper = pepper;
        int finalCorn = corn;
        atTheEndOfTheMonth.add(new HashMap<>() {
            {
                put("Картошка", (beginningOfTheMonth.get(0).get("Картошка") - finalPotato));
                put("Перец", beginningOfTheMonth.get(0).get("Перец") - finalPepper);
                put("Кукуруза", (beginningOfTheMonth.get(0).get("Кукуруза") - finalCorn));
            }
        });
        System.out.println(atTheEndOfTheMonth);
    }
}