package lesson5.task1;

//класс юбка
public class Skirt extends Cloth implements WomensClothing {
    public Skirt(Size clothingSize, int price, String color) {
        super(clothingSize, price, color);
    }

    @Override
    public void dressingWomanInClassClothes() {
        System.out.println("Женщину одели в юбку " + super.color + " цвета, в " + super.clothingSize.getDescription()+" "+super.clothingSize.getEuroSize() + " и стоимостью " + super.price + " $");
    }
}