package lesson5.task1;

//класс ателье
public class Atelier {
    public void dressTheWoman(Cloth cloth) {
        cloth.dressingWomanInClassClothes();
    }

    public void dressTheMan(Cloth cloth) {
        cloth.dressingManInClassClothes();
    }
}
