package lesson5.task1;

//класс Галстук
public class Tie extends Cloth implements MensClothing {
    public Tie(Size clothingSize, int price, String color) {
        super(clothingSize, price, color);
    }

    @Override
    public void dressingManInClassClothes() {
        System.out.println("Мужчину одели в галстук " + super.color + " цвета, в " + super.clothingSize.getDescription() + " " + super.clothingSize.getEuroSize() + " и стоимостью " + super.price + " $");
    }
}
