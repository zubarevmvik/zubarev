package lesson5.task1;

//класс штаны
public class Pants extends Cloth implements WomensClothing, MensClothing {
    public Pants(Size clothingSize, int price, String color) {
        super(clothingSize, price, color);
    }

    @Override
    public void dressingManInClassClothes() {
        System.out.println("Мужчину одели в штаны " + super.color + " цвета, в " + super.clothingSize.getDescription() + " " + super.clothingSize.getEuroSize() + " и стоимостью " + super.price + " $");
    }

    @Override
    public void dressingWomanInClassClothes() {
        System.out.println("Женщину одели в штаны " + super.color + " цвета, в " + super.clothingSize.getDescription() + " " + super.clothingSize.getEuroSize() + " и стоимостью " + super.price + " $");
    }
}


