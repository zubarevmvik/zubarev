package lesson5.task1;

//класс одежды
public abstract class Cloth {
    protected Size clothingSize;
    protected int price;
    protected String color;

    public Cloth(Size clothingSize, int price, String color) {
        this.clothingSize = clothingSize;
        this.price = price;
        this.color = color;
    }


    public void dressingManInClassClothes() {
    }

    public void dressingWomanInClassClothes() {
    }
}
