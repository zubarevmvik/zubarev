package lesson5.task3;

public class Aspirant extends Student {
    public Aspirant(String firstName, String lastName, String group, double averageMark) {
        super(firstName, lastName, group, averageMark);
    }

    @Override
    public void getScholarship() {
        if (super.averageMark == STUDENT_GRADE_EXCELLENT) {
            System.out.println("У аспиранта " + this.firstName + " стипендия 200 гривен");
        } else {
            System.out.println("У аспиранта " + this.firstName + " стипендия 180 грвен");
        }
    }
}
