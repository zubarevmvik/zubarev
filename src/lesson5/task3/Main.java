package lesson5.task3;

import java.util.ArrayList;

/**
 * Создайте пример наследования, реализуйте класс Student и класс Aspirant, аспирант отличается от студента наличием некой научной работы.
 * а) Класс Student содержит переменные: String firstName, lastName, group. А также, double averageMark, содержащую среднюю оценку.
 * б) Создать переменную типа Student, которая ссылается на объект типа Aspirant.
 * в) Создать метод getScholarship() для класса Student, который возвращает сумму стипендии. Если средняя оценка студента равна 5,
 * то сумма 100 грн, иначе 80. Переопределить этот метод в классе Aspirant. Если средняя оценка аспиранта равна 5, то сумма 200 грн, иначе 180.
 * г) Создать массив типа Student, содержащий объекты класса Student и Aspirant. Вызвать метод getScholarship() для каждого элемента массива.
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Aspirant("Смирнов", "Сергей", "46а", 3.4));
        students.add(new Aspirant("Иванов", "Иван", "46а", 5));
        students.add(new Student("Завозин", "Александр", "14г", 3.4));
        students.add(new Student("Петрова", "Романа", "14г", 5));

        for (Student student : students) {
            student.getScholarship();
        }
    }
}
