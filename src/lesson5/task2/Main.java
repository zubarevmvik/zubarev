package lesson5.task2;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 1. Определить интерфейс Printable, содержащий метод void print().
 * 2. Определить класс Book, реализующий интерфейс Printable.
 * 3. Определить класс Magazine, реализующий интерфейс Printable.
 * 4. Создать массив типа Printable, который будет содержать книги и журналы.
 * 5. В цикле пройти по массиву и вызвать метод print() для каждого объекта.
 * 6. Создать статический метод printMagazines(Printable[] printable) в классе Magazine, который выводит на консоль названия только журналов.
 * 7. Создать статический метод printBooks(Printable[] printable) в классе Book, который выводит на консоль названия только книг. Используем оператор instanceof для определения типа.
 */

public class Main {
    public static void main(String[] args) {
//      создаем общий массив для журналов и книг
        Printable[] printables = {new Book("Война и мир", 1992), new Book("Мертвые души", 2006), new Book("Отцы и дети", 1976), new Magazine("Добрые советы", 2020), new Magazine("Караван историй", 2021), new Magazine("Работница", 2022)};

//      Считаем сколько у нас журналов и книг. Выводим информацию по книгам и журналам. Создаем массивы для журналов и книг.
        int magazineSize = 0;
        int bookSize = 0;
        for (Printable printable : printables) {
            if (printable instanceof Magazine) {
                printable.print();
                magazineSize++;
            } else {
                if (printable instanceof Book) {
                    printable.print();
                    bookSize++;
                }
            }
        }
        Printable[] magazine = new Printable[magazineSize];
        Printable[] book = new Printable[bookSize];

//      Создаем массивы
        int indexForMagazines = 0;
        int indexForBook  = 0;
        for (Printable printable : printables) {
            if (printable instanceof Magazine) {
                magazine[indexForMagazines] = printable;
                indexForMagazines++;
            } else {
                if (printable instanceof Book) {
                    book[indexForBook ] = printable;
                    indexForBook ++;
                }
            }
        }

        System.out.println();
        Magazine.printMagazines(magazine);
        Book.printBooks(book);
    }
}
