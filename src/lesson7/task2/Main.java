package lesson7.task2;

public class Main {
    public static void main(String[] args) {
        StudentBuilder student1 = new StudentBuilder()
                .setName("Иван")
                .setSurname("Петров")
                .getStudent();

        StudentBuilder student2 = new StudentBuilder()
                .setUniversity("МГИМО");
        System.out.println(student2.getResult().getUniversity());
    }
}
