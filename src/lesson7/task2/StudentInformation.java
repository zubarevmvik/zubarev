package lesson7.task2;

public interface StudentInformation {
    public StudentBuilder setName(String name);
    public StudentBuilder setSurname(String surname);
    public StudentBuilder setYearOfBirth(String yearOfBirth);
    public StudentBuilder setWell(String well);
    public StudentBuilder setCityOfBirth(String cityOfBirth);
    public StudentBuilder setUniversity(String university);
}
