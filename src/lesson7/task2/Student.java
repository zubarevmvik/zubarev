package lesson7.task2;

public class Student {
    private String name; //имя
    private String surname; //фамилия
    private String yearOfBirth; //год рождения
    private String well; //курс
    private String cityOfBirth; //город рождения
    private String university; //университет

    public Student(String name, String surname, String yearOfBirth, String well, String cityOfBirth, String university) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.well = well;
        this.cityOfBirth = cityOfBirth;
        this.university = university;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getYearOfBirth() {
        return yearOfBirth;
    }

    public String getWell() {
        return well;
    }

    public String getCityOfBirth() {
        return cityOfBirth;
    }

    public String getUniversity() {
        return university;
    }
}

