package lesson7.task2;

public class StudentBuilder implements StudentInformation {
    private String name; //имя
    private String surname; //фамилия
    private String yearOfBirth; //год рождения
    private String well; //курс
    private String cityOfBirth; //город рождения
    private String university; //университет

    @Override
    public StudentBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public StudentBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    @Override
    public StudentBuilder setYearOfBirth(String yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
        return this;
    }

    @Override
    public StudentBuilder setWell(String well) {
        this.well = well;
        return this;
    }

    @Override
    public StudentBuilder setCityOfBirth(String cityOfBirth) {
        this.cityOfBirth = cityOfBirth;
        return this;
    }

    @Override
    public StudentBuilder setUniversity(String university) {
        this.university = university;
        return this;
    }

    public Student getResult() {
        return new Student(this.name, this.surname, this.yearOfBirth, this.well, this.cityOfBirth, this.university);
    }

    public StudentBuilder getStudent() {
        System.out.println("имя " + this.getResult().getName() + ", фамилия " + this.getResult().getSurname());
        return null;
    }
}
