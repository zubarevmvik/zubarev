package lesson7.task1;

public final class Earth {
    private static Earth earth;

    private Earth() {
    }

    public static Earth getEarth() {
        if (earth == null) {
            earth = new Earth();
        }
        return earth;
    }
}

