package lesson7.task1;

/**
 * Написать класс Земля, такой, что у него гарантированно может быть один единственный экземпляр в программе.
 * В комментарии в merge request указать, какой паттерн проектирования вы использовали.
 * (паттерн Singleton\Одиночка)
 */
public class Main {
    public static void main(String[] args) {
        Earth earth = Earth.getEarth();
        System.out.println(earth);
    }
}
